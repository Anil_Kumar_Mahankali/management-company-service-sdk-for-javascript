/**
 * @module
 * @description management company service sdk public API
 */
export {default as ManagementCompanyServiceSdkConfig } from './managementCompanyServiceSdkConfig';
export {default as ManagementCompanySynopsisView} from './managementCompanySynopsisView';
export {default as default} from './managementCompanyServiceSdk';