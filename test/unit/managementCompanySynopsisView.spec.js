import ManagementCompanySynopsisView from '../../src/managementCompanySynopsisView';
import dummy from '../dummy';

describe('ManagementCompanySynopsisView', ()=> {
    describe('constructor', () => {
        it('throws if id is null', () => {
            /*
             arrange
             */
            const constructor =
                () => new ManagementCompanySynopsisView(null, dummy.name);

            /*
             act/assert
             */
            expect(constructor).toThrowError(TypeError, 'id required');
        });
        it('sets id', () => {
            /*
             arrange
             */
            const expectedId = "1";

            /*
             act
             */
            const objectUnderTest =
                new ManagementCompanySynopsisView(expectedId, dummy.name);

            /*
             assert
             */
            const actualId = objectUnderTest.id;
            expect(actualId).toEqual(expectedId);

        });
        it('throws if name is null', () => {
            /*
             arrange
             */
            const constructor =
                () => new ManagementCompanySynopsisView(dummy.id, null);

            /*
             act/assert
             */
            expect(constructor).toThrowError(TypeError, 'name required');
        });

        it('sets name', () => {
            /*
             arrange
             */
            const expectedName = dummy.name;

            /*
             act
             */
            const objectUnderTest =
                new ManagementCompanySynopsisView(dummy.id, expectedName);

            /*
             assert
             */
            const actualName = objectUnderTest.name;
            expect(actualName).toEqual(expectedName);

        });
    })
});

