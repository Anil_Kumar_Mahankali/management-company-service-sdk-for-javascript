import ManagementCompanyServiceSdk,
{
ManagementCompanySynopsisView
} from  '../../src/index';
import  factory from './factory';
import config from './config';
//import dummy from '../dummy';

/*
tests
 */

describe('Index module', () => {


    describe('default export', () => {
        it('should be ManagementCompanyServiceSdk constructor', () => {
            /*
             act
             */
            const objectUnderTest =
                new ManagementCompanyServiceSdk(
                    config.managementCompanyServiceSdkConfig
                );

            /*
             assert
             */
            expect(objectUnderTest).toEqual(jasmine.any(ManagementCompanyServiceSdk));

        });
    });

        describe('listManagementCompanies method', () => {
                it('should return more than 1 result', (done) => {
                    /*
                     arrange
                     */
                    const objectUnderTest =
                        new ManagementCompanyServiceSdk(
                            config.managementCompanyServiceSdkConfig
                        );


                    /*
                     act
                     */
                    const listManagementCompaniesSynopsisViewsPromise =
                        objectUnderTest
                            .listManagementCompanies(
                                factory.constructValidAppAccessToken()
                            );

                    /*
                     assert
                     */
                    listManagementCompaniesSynopsisViewsPromise
                        .then((ManagementCompanySynopsisView) => {
                            expect(ManagementCompanySynopsisView).toBeTruthy();
                            done();
                        })
                        .catch(error=> done.fail(JSON.stringify(error)));

                }, 20000);
            });

    });